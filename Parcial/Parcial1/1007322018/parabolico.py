from sqlite3 import Time
import numpy as np
import matplotlib.pyplot as plt

class Parabolico:
    
    def __init__(self,alpha,v0,g,ax):
         self.alpha= alpha #angulo inicial
         self.v0 = v0 #velocidad inicial
         self.g= g #gravedad
         self.ax= ax #aceleracion

    def tiempo(self):

        self.t = (-2*self.v0*np.sin(self.alpha))/self.g #tiempo de vuelo
        self.T = np.linspace(0,self.t,100)

        return self.T

    def TiempodeVuelo(self):
        self.Time = (-2*self.v0*np.sin(self.alpha))/self.g
        return self.Time


    def distancia(self):
        self.x = self.v0*np.cos(self.alpha)*self.tiempo()+0.5*(self.tiempo()*self.tiempo())*self.ax 
        self.y = self.v0*np.sin(self.alpha)*self.tiempo()+0.5*(self.tiempo()*self.tiempo())*self.g 

        return self.x,self.y        #Trayectoria en X y Y

    def Vx(self): 
        v0x = np.linspace(self.v0*np.cos(self.alpha),self.v0*np.cos(self.alpha),len(self.tiempo())) 
        self.vx = v0x+self.ax*self.tiempo()
        return self.vx   #Velocidad en X durante todo el trayecto
  
    def Vy(self):
        v0y = np.linspace(self.v0*np.sin(self.alpha),self.v0*np.sin(self.alpha),len(self.tiempo())) 
        self.vy = v0y+self.g*self.tiempo()
        return self.vy  #Velocidad en Y durante todo el trayecto         

    def AlcanceMax(self): 
        self.Alcance = self.v0*np.cos(self.alpha)*self.tiempo()[0]+0.5*(self.tiempo()[0]*self.tiempo()[0])*self.ax  
        return self.Alcance # X maximo 

    def AlturaMax(self): 
        self.t2 = (-self.v0*np.sin(self.alpha))/self.g
        self.altura = self.v0*np.sin(self.alpha)*self.t2+0.5*(self.t2*self.t2)*self.ax
        return self.altura #Altura maximo


class GRAFICA(Parabolico): #Esta clase es para graficar la trayectoria

    def __init__(self,alpha,v0,g,ax):
        super().__init__(alpha,v0,g,ax)
    


        
    def Grafica(self):

         plt.plot(self.distancia()[0],self.distancia()[1])
         plt.xlabel('X',size=30)
         plt.ylabel('Y',size=30)
         plt.title('TRAYECTORIA PARABOLICA CON ACELERACION DEL VIENTO', size=24)
         plt.show()  
      
