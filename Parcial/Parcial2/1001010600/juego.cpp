#include <iostream>
#include <stdlib.h>
#include <ctime>
using namespace std;

bool adivinarNum(int uGuess, int num)
{
    if (uGuess == num)
    { // Condición de ganar el juego
        cout << "Ganaste!" << endl;
        bool condition = false;
        return condition;
    }
    else if (uGuess < num) // Condiciones si el numero ingresa es mayor o menor
    {
        cout << "El número es mayor" << endl;
        bool condition = true;
        return condition;
    }
    else
    {
        cout << "El número es menor" << endl;
        bool condition = true;
        return condition;
    }
}

int main()
{
    srand(time(NULL));                 // Semilla aleatoria
    bool condition = true;             // Condicion de parada para el ciclo while
    int uGuess;                        // Almacena el numero del usuario
    int num = 1 + rand() % (1000 - 1); // Genera un numero aleatorio entre 1 y 1000
    string pAgain;                     // Variable para saber si el usuario desea jugar nuevamente

    cout << "Juego de adivinar número" << endl;
    cout << "Introduzca un número entre 1 y 1000: " << endl;

    while (condition == true)
    {
        cin >> uGuess; // Almacena el número ingresado
        condition = adivinarNum(uGuess, num);

        if (condition == false) // Opcion de jugar de nuevo
        {
            cout << "¿Desea jugar nuevamente?" << endl
                 << "(Si/No)" << endl;

            cin >> pAgain;
            if (pAgain == "Si")
            {
                condition = true;
                num = 1 + rand() % (1000 - 1);
                cout << endl
                     << "Nueva partida, adivina el número!" << endl;
            }
            else
            {
                continue;
            }
        }
    }

    return 0;
}