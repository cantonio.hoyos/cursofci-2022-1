/* 
 Este código realiza la ejecución de un juego donde el usuario debe adivinar un número entre el 1 y el 1000 que genera automaticamente la computadora
 */

#include <iostream> 
#include <stdlib.h> 

using namespace std;

//En primer lugar se crea la función principal
void AdivinarNumero(int A){
    
    //Se genera el número aleatorio
    srand(time(NULL));
    int random = rand() % 1000 + 1;
    
    //Se inicializa un boolenao que determinará cuándo se detiene el juego
    bool guess = false;
    
    //Con un ciclo while se le indica en cada paso al usuario si el número es mayor, menor o ha ganado
    while(guess == false){
        if(A<random){
            cout << "El numero que intenta adivinar es mayor que el ingresado, ingrese un nuevo numero\n";
            cin >> A;
        }
        if(A>random){
            cout << "El numero que intenta adivinar es menor que el ingresado, ingrese un nuevo numero\n";
            cin >> A;
        }
        if(A==random){
            cout << "El numero es identico, felicitaciones" << endl;
            guess = true;
        }
    }
}

int main(){
    int A;
    
    //Se le pide al usuario el número inicial y se ejecuta la función
    cout << "Ingrese un número entre 1 y 1000\n";
    cin >> A;
    AdivinarNumero(A);
    
    //Se le pregunta al usuario si deres jugar de nuevo
    int respuesta;
    cout << "¿Quiere jugar otra vez?   Sí = 1   No = 0\n";
    cin >> respuesta;
    
    if(respuesta==1){
        main();
    }
    return 0;
}
