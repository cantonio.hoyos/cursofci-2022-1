from dados import dados
import matplotlib.pyplot as plt

if __name__ == '__main__':
    
    #Dos dados
    dos = dados(2)
    x2s,y2s = dos.distribucion_simultaneos()
    
    #Tres dados
    tres = dados(3)
    x3s,y3s = tres.distribucion_simultaneos()
    
    #Cuatro dados
    cuatro = dados(4)
    x4s,y4s = cuatro.distribucion_simultaneos()

    #Cinco dados
    cinco = dados(5)
    x5s,y5s = cinco.distribucion_simultaneos()

    #Graficas de los datos obtenidos
    dos.grafica(x2s,x3s,x4s,x5s,y2s,y3s,y4s,y5s, 'Tiros Simultaneos')
