import punto22 as p22
import matplotlib.pyplot as plt
import numpy as np

# Inicialización.
if __name__=='__main__':
    print('Inicialización')

# Creación de objeto
N = 10000
acelerador = p22.LHC(100,10,10000)
experimento = acelerador.Random_position()
experimento1 = acelerador.choque()

intentos = np.linspace(0,N,len(experimento1))
plt.plot(intentos, experimento1)
plt.title("Probabilidad de colisión con R = 100 y r= 10 en 10mil intentos")
plt.xlabel("Intentos")
plt.ylabel("Probabilidad")
plt.show()