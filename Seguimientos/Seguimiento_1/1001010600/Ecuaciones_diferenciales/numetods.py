from sympy import *

class metodosNumericos:
    #Metodo constructor
    def __init__(self, x0, y0, a, b, n, fun, x):
        #Inicializamos los parametros
        self.x0 = x0
        self.y0 = y0
        self.a = a
        self.b = b
        self.n = n
        self.fun = lambda x, y: eval(fun)
        self.Sfun = fun
        self.x = x

    def euler(self):

        h = (self.b-self.a)/self.n
        x = self.x0
        y = self.y0
        #Calculo de yn+1
        while abs(x-self.x)>0.0001:
            y = y + h*self.fun(x, y)
            x = h + x
        return y

    def RK4(self):

        h = (self.b-self.a)/self.n
        x = self.x0
        y = self.y
        #Funciones de Runge Kuta
        def K1(x, y):
            return h * self.fun(x, y)

        def K2(x, y):
            return h * self.fun(x+0.5*h, y + 0.5*K1(x, y))

        def K3(x, y):
            return h * self.fun(x+0.5*h, y + 0.5*K2(x, y))

        def K4(x, y):
            return h * self.fun(x + h, y + K3(x, y))
        #Calculo de yn+1
        while abs(x-self.x)>0.0001:
            y = y + (1/6)*( K1(x, y) + 2*K2(x, y) + 2*K3(x, y) + K4(x, y))
            x = h + x
        return y
    
    def analitic(self):
        x = symbols('x')
        y = Function('y')(x)
        fun = sympify(self.Sfun)
        eq = Eq(y.diff(x), fun)
        result = dsolve(eq)
        return result
