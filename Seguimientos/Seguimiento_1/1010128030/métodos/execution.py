from metodos import metodos_numericos

if __name__ == '__main__':
    
    #Defina su función
    def function(x,y):
        return 2*x*y
    
    #Defina su evaluación en el siguiente orden (y0, x0, paso, funcion, evaluacion_x)
    func = metodos_numericos(y0 = 1, x0 = 1, paso = 0.1, funcion = function, evaluacion_x = 1.5)
    
    print(func.mensaje_euler())
    print(func.mensaje_rk4())
    print(func.analitico())
