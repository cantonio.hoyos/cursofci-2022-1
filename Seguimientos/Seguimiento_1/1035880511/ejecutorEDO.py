# Se importa el archivo de clases y librerías.

import EDO as EDO
import numpy as np
import sympy as sp
sp.init_printing()

# Inicialización.
if __name__=='__main__':
    print('Inicialización')

# Se definen algunas funciones. 

def f2(x,y):
    return y*(x)**(0.5)

def f3(x,y):
    return 2*x*y

def f4(x,y):
    return 2*y +x


# Se crean los objetos para implementar con la clase métodos

respf2=EDO.Metodos(0.001,0,2,0,1,f2,1.5)
respf3= EDO.Metodos(0.1,0,5,1,1,f3,2)
respf4= EDO.Metodos(1,0,5,1,1,f4,2.5)

# Se imprimen los resultados. 

#print(respf2.meuler())
#print(respf2.rk4())
#print(respf3.meuler())
print(respf3.rk4())


# Implementación del método analítico con Sympy con la función sugerida f3
x = sp.symbols('x')
y = sp.Function('y')(x)
dydx = y.diff(x)
expr = sp.Eq(dydx, f3(x,y))
print("La solución por método analítico Sympy es: {}".format(sp.dsolve(expr)))







    




  
  


