import numpy as np
import matplotlib.pyplot as plt

class particula:


# 1)METODO CONSTRUCTOR:
    def __init__(self,E,m,q,B,theta):
        self.E=E*1.6022e-19
        self.m=m
        self.q=q
        self.B=B
        self.theta=theta*(np.pi/180)

        self.w=(self.q*self.B)/self.m
        self.t=np.arange(0,10,0.1)

# VELOCIDAD V0
    def v0(self):
        v0=np.sqrt((2*self.E)/self.m)
        return v0
        
# VELOCIDAD EN X:  
    #def vx(self):
        #vx=self.v0*np.cos(w*t)
        #return vx

# VELOCIDAD EN Y:
    #def vy(self):
        #vy=-self.v0*np.sin(w*t)
        #return vy

# POSICION EN X:
    def x(self):
        x=(self.v0()*np.sin(self.theta)/self.w)*(np.sin(self.w*self.t))
        #x=(self.v0/w)*np.sin(w*t)
        return x
        
# POSICION EN Y:
    def y(self):
        y=((self.v0()*np.sin(self.theta))/self.w)*(np.cos(self.w*self.t)-1)
        #y=(self.v0/w)*(np.cos(w*t))-1
        return y

    def z(self):
        z=self.v0()*np.cos(self.theta)*self.t
        return z

# GRAFICAS

    # t VS x
    def xgrafica(self):
        plt.plot(self.t,self.x())
        plt.xlabel("t")
        plt.ylabel("x")
        plt.title("TRAYECTORIA  t vs X")
        plt.show()

    # t VS Y
    #def ygrafica(self):
        #plt.plot(self.t,self.y())
        #plt.xlabel("t")
        #plt.ylabel("y")
        #plt.title("TRAYECTORIA  t vs y")
        #plt.show()

    # t VS 
    #def zgrafica(self):
        #plt.plot(self.t,self.z())
        #plt.xlabel("t")
        #plt.ylabel("z")
        #plt.title("TRAYECTORIA  t vs Z")
        #plt.show()