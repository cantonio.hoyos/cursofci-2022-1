import numpy as np


class parabolico:

    def __init__(self,hi,v0,theta,t):
        self.hi=hi
        self.v0=v0
        self.theta=theta*(np.pi/180)
        self.t=t


    def vx(self):
        vx=self.v0*np.cos(self.theta)
        return vx
    
    def x(self):
        return self.vx()*self.t

    def vy(self):
        vy=self.v0*(np.sin(self.theta))
        return vy
    





